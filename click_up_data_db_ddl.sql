create table public.auth_token
(
	token text not null
		constraint auth_token_pk
			primary key
);

alter table public.auth_token owner to postgres;

create table public.team
(
	id text not null
		constraint team_pk
			primary key,
	name text not null
);

alter table public.team owner to postgres;

create unique index team_name_uindex
	on public.team (name);

create table public.click_up_user
(
	id integer not null
		constraint click_up_user_pk
			primary key,
	username text,
	email text not null,
	initials text,
	role integer
);

alter table public.click_up_user owner to postgres;

create unique index click_up_user_email_uindex
	on public.click_up_user (email);

create table public.space
(
	id text not null
		constraint space_pk
			primary key,
	name text not null,
	team_id text not null
);

alter table public.space owner to postgres;

create unique index space_name_uindex
	on public.space (name);

create table public.folder
(
	id text not null
		constraint folder_pk
			primary key,
	name text not null,
	space_id text not null
);

alter table public.folder owner to postgres;

create table public.task
(
	id text not null
		constraint task_pk
			primary key,
	name text not null,
	description text,
	parent_id text,
	url text not null,
	status text not null,
	author_id integer not null,
	list_id text not null,
	custom_id text,
	tags text,
	date_created_unix text not null,
	date_created text not null,
	customers text,
	is_approved boolean not null,
	direction_of_effort text,
	understanding_the_value text,
	deliverable_id text,
	product_id text
);

alter table public.task owner to postgres;

create unique index task_url_uindex
	on public.task (url);

create table public.time
(
	id text not null
		constraint time_pk
			primary key,
	description text,
	time_hours double precision not null,
	date_added text not null,
	start_date text not null,
	end_date text not null,
	task_id text not null,
	user_id integer not null,
	time_unix text not null,
	date_added_unix text not null,
	start_date_unix text not null,
	end_date_unix text not null,
	billable boolean
);

alter table public.time owner to postgres;

create table public.list
(
	id text not null
		constraint list_pk
			primary key,
	name text not null,
	space_id text not null,
	folder_id text
);

alter table public.list owner to postgres;

create table public.task_user
(
	task_id text not null,
	click_up_user_id integer not null,
	constraint task_user_pk
		primary key (task_id, click_up_user_id)
);

alter table public.task_user owner to postgres;

create table public.deliverable_task
(
	id text not null
		constraint deliverable_task_pk
			primary key,
	name text not null,
	description text,
	parent_id text,
	url text not null,
	status text not null,
	author_id integer not null,
	list_id text not null,
	custom_id text,
	tags text,
	date_created_unix text not null,
	date_created text not null,
	customers text,
	is_approved boolean not null,
	direction_of_effort text,
	understanding_the_value text,
	deliverable_id text,
	product_id text
);

alter table public.deliverable_task owner to postgres;

create unique index deliverable_task_url_uindex
	on public.deliverable_task (url);

create table public.product_task
(
	id text not null
		constraint product_task_pk
			primary key,
	name text not null,
	description text,
	parent_id text,
	url text not null,
	status text not null,
	author_id integer not null,
	list_id text not null,
	custom_id text,
	tags text,
	date_created_unix text not null,
	date_created text not null,
	customers text,
	is_approved boolean not null,
	direction_of_effort text,
	understanding_the_value text,
	deliverable_id text,
	product_id text
);

alter table public.product_task owner to postgres;

create unique index product_task_url_uindex
	on public.product_task (url);

create table public.output_task
(
	id text not null
		constraint output_task_pk
			primary key,
	name text not null,
	description text,
	parent_id text,
	url text not null,
	status text not null,
	author_id integer not null,
	list_id text not null,
	custom_id text,
	tags text,
	date_created_unix text not null,
	date_created text not null,
	customers text,
	is_approved boolean not null,
	direction_of_effort text,
	understanding_the_value text,
	deliverable_id text,
	product_id text
);

alter table public.output_task owner to postgres;

create unique index output_task_url_uindex
	on public.output_task (url);

CREATE OR REPLACE FUNCTION handle_deliverable()
    RETURNS void AS
$$
BEGIN
    truncate deliverable_task;

    INSERT INTO deliverable_task
    SELECT *
    FROM task t
    WHERE t.tags like '%deliverable%';
END;
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION handle_product()
    RETURNS void AS
$$
BEGIN
    truncate product_task;

    INSERT INTO product_task
    SELECT *
    FROM task t
    WHERE t.tags like '%product%';
END;
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION handle_output()
    RETURNS void AS
$$
BEGIN
    truncate output_task;

    INSERT INTO output_task
    SELECT *
    FROM task t
    WHERE t.tags like '%output%';
END;
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION handle_autocreated()
    RETURNS void AS
$$
BEGIN
    update task
    set name      = concat('autocreated - ', name),
        parent_id = id,
        tags      = null
    where id in (select distinct o.id
                 from output_task o
                          join time t on o.id = t.task_id);
END;
$$
    LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION handle_clear()
    RETURNS void AS
$$
BEGIN
    delete
    from task
    where tags like '%deliverable%'
       or tags like '%product%'
       or tags like '%output%';
END;
$$
    LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION handle()
    RETURNS void AS
$$
BEGIN
    perform handle_deliverable();
    perform handle_product();
    perform handle_output();
    perform handle_autocreated();
    perform handle_clear();
END;
$$
    LANGUAGE plpgsql;

insert into public.auth_token values ('pk_8953580_OIDOEGPWDNRFU3FFEGF7TIM3RAZ9NZ4F');
